<footer class="footer">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="footer__copy">
                    © 2004-2016 Сибирские cети
                </div>
                <div class="footer__help">
                    Нужна помощь? Обратитесь к нам по телефону <a href="tel:8001009692">800-100-9692</a>
                </div>
            </div>
        </div>
    </div>
</footer>
<div id="popup1" class="modal fade" role="dialog">
    <div class="modal-dialog modal-popup-1">
        <div class="modal-content">
            <button type="button" class="close close-popup1" data-dismiss="modal">&times;</button>
            <div class="modal-body">
                <div class="modal-logo">
                    <img class="img-responsive" src="<?php bloginfo('template_url'); ?>/images/modal-logo.png" alt="">
                </div>
                <h3 class="modal-h3">Подключиться</h3>
                <form id="form_connection" action="">
                    <input type="tel" class="number_phone" name="user_phone" maxlength="12" placeholder="Телефон">
                    <p class="send-processing">Отправка номера телефона <i class="fa fa-spinner fa-spin fa-fw"></i></p>
                    <p class="error-form">*Введите номер телефона</p>
                    <p class="success-form">Мы перезвоним вам в течение часа и назначим подключение на удобное вам время</p>
                    <input class="btn btn-send-form btn-green" type="submit" value="Отправить заявку">
                </form>
            </div>
        </div>
    </div>
</div>

<script src="<?php bloginfo('template_url'); ?>/modules/jquery-3.1.1/jquery-3.1.1.min.js"></script>
<script src="<?php bloginfo('template_url'); ?>/modules/bootstrap-3.3.7/dist/js/bootstrap.min.js"></script>
<script src="<?php bloginfo('template_url'); ?>/modules/jquery-parallax-scroll/jquery.parallax-scroll.js"></script>
<script src="<?php bloginfo('template_url'); ?>/js/script.js"></script>
<?php wp_footer();?>
</body>
</html>