<?php
    function print_rates($category_name) {
    ?>
    <div class="prices__tab-title">
        Выберите размер видеоархива:
        <ul>
            <?php
                $categories = get_category_rate($category_name);
                foreach($categories as $key => $value) {
                    ?>
                    <li <?php if($key == 0){ echo 'class="active"'; } ?>>
                        <a href="#<?php echo $value["slug"]."-".$category_name; ?>" data-toggle="tab">
                            <b><?php echo $value["name"]; ?></b>
                        </a>
                    </li>
                    <?php
                }
            ?>
        </ul>
    </div>
    <div class="tab-content">
        <?php
        $rates_value = get_rate_value($categories);
        $count = 0;
        foreach($rates_value as $key_rates => $rates) {
            ?>
            <div id="<?php echo $key_rates."-".$category_name; ?>" class="tab-pane <?php if($count == 0) {echo "active";}?>">
                <?php
                if(sizeof($rates) >= 3) {
                    $coll = '<div class="col-xs-12 col-md-4">';
                } else if(sizeof($rates) == 2) {
                    $coll = '<div class="col-xs-12 col-md-6">';
                } else {
                    $coll = '<div class="col-xs-12 col-md-12">';
                }
                foreach($rates as $key_rate => $rate) {
                    echo $coll;
                    ?>
                        <div class="prices-card">
                            <div class="prices-card__header">
                                <?php echo $rate["title"]; ?>
                            </div>
                            <div class="prices-card__round">
                                <img src="<?php echo $rate["image_url"]; ?>" alt="">
                            </div>
                            <div class="prices-card__cost">
                                <h3><?php echo $rate["quality"]; ?></h3>
                                <h4><?php echo $rate["price"]; ?></h4>
                            </div>
                        </div>
                    </div>
                    <?php
                }

                $count++;
                ?>
            </div>
            <?php
        }
        ?>
    </div>
<?php
}