(function() {
    function phoneInput() {
        $('input[type="tel"]').keypress(function(e) {
            if(e.which != 8) {
                if(e.which < 48 || e.which > 57) { return false;
                }
            }
        });
    }

    $(document).ready(function(){
        phoneInput();

        $(".js-btn-more").click(function(){
            if($(this).siblings(".cams-block__wrap").hasClass("more") == true) {
                $(this).siblings(".cams-block__wrap").removeClass("more");
                $(this).text("подробнее");
            }
            else {
                $(this).siblings(".cams-block__wrap").addClass("more");
                $(this).text("назад");
            }
        });

        $('a[data-attr="anchor"]').click(function(e) {
            var id = $(this).attr('href');
            var $id = $(id);
            var pos = $(id).offset().top - $('.navbar-header').height();

            if ($id.length === 0) {
                return;
            }

            e.preventDefault();
            $('.navbar-collapse').collapse("hide");
            $('body, html').animate({scrollTop: pos}, 650);
        });

        $('.close-popup1').click(function () {
            $('.success-form').removeClass('active-success');
            $('.error-form').removeClass('active-err');
            $('.send-processing').removeClass('active-send-processing');
            $('.number_phone').val("");
        });

        $("#form_connection").submit(function(e) {
            e.preventDefault();
            var number_phone = $('.number_phone').val();

            if(number_phone != "") {
                $('.error-form').removeClass('active-err');
                $('.send-processing').addClass('active-send-processing');
                $('.btn-send-form').css("display", "none");
                var $form_data = new FormData(this);

                $.ajax({
                    type: "POST",
                    url: "/wp-content/themes/video/ajax/send_phone.php",
                    data: $form_data,
                    dataType: "json",
                    processData: false,
                    contentType: false,
                    success: function (data) {
                        if(data["state"] == "success") {
                            $('.error-form').removeClass('active-err');
                            $('.success-form').addClass('active-success');
                            $('.send-processing').removeClass('active-send-processing');
                            $('.btn-send-form').css("display", "block");
                            $('.number_phone').val("");
                        }

                        else if (data["state"] == "err_send" || data["state"] == "err") {
                            $('.success-form').removeClass('active-success');
                            $('.error-form').text("*Ошибка при отправке сообщения");
                            $('.error-form').addClass('active-err');
                            $('.btn-send-form').css("display", "block");
                            $('.send-processing').removeClass('active-send-processing');
                        }
                    },
                    error: function() {
                        $('.success-form').removeClass('active-success');
                        $('.error-form').text("*Ошибка при отправке сообщения");
                        $('.error-form').addClass('active-err');
                        $('.btn-send-form').css("display", "block");
                        $('.send-processing').removeClass('active-send-processing');
                    }
                });

            } else {
                $('.success-form').removeClass('active-success');
                $('.error-form').addClass('active-err');
                $('.send-processing').removeClass('active-send-processing');
            }

        });
    });
    $(document.body).on('show.bs.modal', function () {
        if (this.clientHeight <= window.innerHeight) {
            return;
        }
        // Get scrollbar width
        var scrollbarWidth = getScrollBarWidth()
        if (scrollbarWidth) {
            $('header').css('padding-right', scrollbarWidth);
            $('.section-btn').css('right', scrollbarWidth);
        }
    })
        .on('hidden.bs.modal', function () {
            $('header').css('padding-right', 0);
            $('.section-btn').css('right', 0);
        });

    function getScrollBarWidth () {
        var inner = document.createElement('p');
        inner.style.width = "100%";
        inner.style.height = "200px";

        var outer = document.createElement('div');
        outer.style.position = "absolute";
        outer.style.top = "0px";
        outer.style.left = "0px";
        outer.style.visibility = "hidden";
        outer.style.width = "200px";
        outer.style.height = "150px";
        outer.style.overflow = "hidden";
        outer.appendChild (inner);

        document.body.appendChild (outer);
        var w1 = inner.offsetWidth;
        outer.style.overflow = 'scroll';
        var w2 = inner.offsetWidth;
        if (w1 == w2) w2 = outer.clientWidth;

        document.body.removeChild (outer);

        return (w1 - w2);
    };
    $(".header-btn").click(function(){
        $('.navbar-collapse').collapse("hide");
    });
})($);