<?php get_header(); ?>
<section class="home">
    <div class="container">
        <div class="col-xs-12 col-sm-9 col-md-6 home__block">
            <h1 class="home__h1">Видеоконтроль</h1>
            <div class="home__text">
                <p>Мы устанавливаем IP- видеокамеру и подключаем к облачной платформе</p>
                <p>Вы авторизуетесь на сайте и просматриваете свои камеры через браузер
                    или мобильные приложения</p>
            </div>
        </div>
    </div>
</section>
<section class="about" id="about">
    <div class="container">
        <h2><b>Видеоконтроль</b> через интернет</h2>
        <div class="col-xs-12">
            <?php
            echo get_description();
            ?>
        </div>
        <div class="row">
            <?php
            $haracteristics = get_haracteristics();

            foreach ($haracteristics as $key => $value) {
                ?>
                <div class="about__col col-xs-12 col-sm-3">
                    <div class="about__round">
                        <img src="<?php echo $value["img_url"]; ?>"
                             alt="" class="about__img">
                    </div>
                    <h3 class="about__desc"><?php echo $value["name"]; ?></h3>
                </div>
                <?php
            }
            ?>
        </div>

    </div>
</section>
<section class="prices" id="prices">
    <img src="<?php bloginfo('template_url'); ?>/images/prices.jpg" alt=""
         class="prices-img-parallax" data-parallax='{"y" : 205}'>
    <div class="container">
        <?php
        $home_rates_id = "home";
        $business_rates_id = "business";
        ?>
        <h2 class="prices__h2">Наши <b>тарифы</b></h2>
        <ul class="prices__tab-pill">
            <li class="prices__tab-pill-item active">
                <a href="#<?php echo $home_rates_id; ?>" data-toggle="tab">Для дома</a>
            </li>
            <li class="prices__tab-pill-item">
                <a href="#<?php echo $business_rates_id; ?>" data-toggle="tab">Для бизнеса</a>
            </li>
        </ul>
        <div class="tab-content row">
            <?php
            $name_category_rates_home = "cat_rates_for_home";
            $name_category_rates_business = "cat_rates_for_business";
            ?>
            <div class="tab-pane fade in active" id="<?php echo $home_rates_id; ?>">
                <?php print_rates($name_category_rates_home); ?>
            </div>

            <div class="tab-pane fade" id="<?php echo $business_rates_id; ?>">
                <?php print_rates($name_category_rates_business); ?>
            </div>
        </div>
    </div>

</section>
<button class="btn btn-green section-btn" data-toggle="modal" data-target="#popup1">подключить
</button>
<section class="cams" id="cams">
    <div class="container">
        <h2>Выберите свою <b>камеру</b></h2>
        <div class="row">
            <?php
            $camcorder = get_camcorder();
            foreach ($camcorder as $key => $camera) { ?>
                <div class="col-xs-12 col-md-6">
                    <div class="cams-block">
                        <div class="cams-block__wrap">
                            <div class="cams-block__img-wrap">
                                <img src="<?php echo $camera["img_url"]; ?>" alt=""
                                     class="cams-block__img">
                            </div>
                            <h3 class="cams-block__title">
                                <?php echo $camera["name"]; ?>
                            </h3>
                            <div class="cams-block__desc">
                                <?php echo $camera["description"]; ?>
                            </div>
                            <div class="cams-block__text">
                                <p><b>Матрица: </b><?php echo $camera["matrix"]; ?></p>
                                <p><b>Максимальное
                                        разрешение: </b><?php echo $camera["maximum_resolution"]; ?>
                                </p>
                                <p><b>Объектив: </b><?php echo $camera["lens"]; ?></p>
                                <p><b>Угол обзора по
                                        диагонали: </b><?php echo $camera["viewing_angle"]; ?></p>
                                <p><b>Дальность и угол
                                        ИК: </b><?php echo $camera["distance_angle"]; ?></p>
                            </div>
                        </div>
                        <button class="btn cams__btn-more js-btn-more">подробнее</button>
                    </div>
                </div>
                <?php
            }
            ?>
        </div>
    </div>

</section>
<button class="btn btn-green section-btn" data-toggle="modal" data-target="#popup1">подключить
</button>
<?php
$demo_iframe = get_stream_demo();
if (!empty($demo_iframe)) {
    ?>
    <section class="demo" id="demo">
        <div class="container-fluid demo-container">
            <h2 class="demo__h2">Посмотрите <b>демо</b></h2>
            <?php echo $demo_iframe; ?>
        </div>
    </section>
<?php } ?>
<?php get_footer(); ?>
