<!DOCTYPE html>
<html>
<head>
    <title><?php bloginfo('title'); ?><?php echo "-"; bloginfo('description'); ?></title>
    <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800&amp;subset=cyrillic" rel="stylesheet">
    <link href="<?php bloginfo('template_url'); ?>/modules/bootstrap-3.3.7/dist/css/bootstrap.min.css" rel="stylesheet" media="screen">
    <link href="<?php bloginfo('template_url'); ?>/modules/font-awesome-4.7.0/css/font-awesome.min.css" rel="stylesheet" media="screen">
    <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/main_style.css">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <?php wp_head();?>
</head>
<body data-offset="85" data-target=".navbar" data-spy="scroll">
    <header>
        <nav class="navbar" role="navigation">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#"><img class="img-responsive" src="<?php bloginfo('template_url'); ?>/images/header-logo.png" alt=""></a>
                </div>

                <div class="collapse navbar-collapse navbar-ex1-collapse">
                    <ul class="nav navbar-nav">
                        <li><a data-attr="anchor" href="#prices">Тарифы</a></li>
                        <li><a data-attr="anchor" href="#cams">Камеры</a></li>
                        <li><a data-attr="anchor" href="#demo">Демо</a></li>
                        <li><a href="<?php echo get_link_account(); ?>">Войти</a></li>
                        <button class="btn btn-green header-btn" data-toggle="modal" data-target="#popup1">Подключить</button>
                    </ul>
                </div>
            </div>
        </nav>
    </header>