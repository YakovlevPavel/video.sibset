<?php
    require_once "../../../../wp-load.php";

    $user_phone = $_POST['user_phone'];
    $data = array();

    if(empty($user_phone)) {
        $data["state"] = "err";
    } else {
        if(send_phone($user_phone)) {
            $data["state"] = "success";
        } else {
            $data["state"] = "err_send";
        }
    }

    echo json_encode($data);
?>