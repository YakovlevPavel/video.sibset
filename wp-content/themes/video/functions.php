<?php
    require 'template_functions/rates.php';
    require 'modules/PHPMailer/PHPMailerAutoload.php';

    function print_object($object) {
        echo '<pre>';
            print_r($object);
        echo '</pre>';
    }

    function get_description() {
        $description = pods("content_site");
        return $content = $description->field('content');
    }

    function get_haracteristics() {
        $haracteristics_pod = pods('haracteristics', array("limit" => -1,));

        $haracteristics = array();
        $i = 0;

        if($haracteristics_pod->total() > 0) {
            while($haracteristics_pod->fetch()) {
                $image = $haracteristics_pod->field('image');

                $haracteristics[$i] = array(
                    "name" => $haracteristics_pod->field('name'),
                    "img_url" => $image['guid'],
                );

                $i++;
            }
        } else {
            return NULL;
        }

        return $haracteristics;
    }

    function get_category_rate($cat_name) {
        $terms = get_terms($cat_name);
        $categories=array();

        foreach($terms as $key => $value) {
            $categories[$key] = array(
                "id" => $value->term_id,
                "term_taxonomy_id" => $value->term_taxonomy_id,
                "taxonomy" => $value->taxonomy,
                "name" => $value->name,
                "slug" => $value->slug,
            );
        }

        return $categories;
    }

    function get_rate_value($categories) {
        $rate_value = array();

        foreach($categories as $key => $category) {

            $params = array(
                'tax_query' => array(
                    array(
                        'taxonomy' => $category["taxonomy"],
                        'field' => 'slug',
                        'terms' => $category["slug"]
                    )
                )
            );
            $query = new WP_Query( $params );
            $i = 0;
            $rates = array();
            if ( $query->have_posts() ) {
                while ( $query->have_posts() ) {
                    $query->the_post();
                    $id = get_the_ID();
                    $image = get_post_meta($id, 'image', true);

                    $rates[$i] = array(
                        "title" => get_the_title(),
                        "image_url" => $image["guid"],
                        "quality" => get_post_meta($id, 'quality', true),
                        "price" => get_post_meta($id, 'price', true)
                    );
                    $i++;
                }

            }

            wp_reset_postdata();
            $rate_value[$category["slug"]] = $rates;
        }

        return $rate_value;
    }

    function get_camcorder() {
        $camcorder_pod = pods('camera', array("limit" => -1,));

        $camcorder = array();
        $i = 0;

        if($camcorder_pod->total() > 0) {
            while($camcorder_pod->fetch()) {
                $image = $camcorder_pod->field('image');

                $camcorder[$i] = array(
                    "name" => $camcorder_pod->field('name'),
                    "img_url" => $image['guid'],
                    "description" => $camcorder_pod->field('description'),
                    "matrix" => $camcorder_pod->field('matrix'),
                    "maximum_resolution" => $camcorder_pod->field('maximum_resolution'),
                    "lens" => $camcorder_pod->field('lens'),
                    "viewing_angle" => $camcorder_pod->field('viewing_angle'),
                    "distance_angle" => $camcorder_pod->field('distance_angle'),
                );

                $i++;
            }
        } else {
            return NULL;
        }

        return $camcorder;
    }

    function get_stream_demo() {
        $description = pods("content_site");
        return $content = $description->field('iframe');
    }

    function get_link_account() {
        $description = pods("content_site");
        return $content = $description->field('link_account');
    }

    function set_settings_smtp() {
        $site_settings = pods('email_settings');
        $smtp_host = $site_settings->field('host_smtp');
        $smtp_username = $site_settings->field('username_smtp');
        $smtp_password = $site_settings->field('password_smtp');
        $smtp_port = $site_settings->field('port_smtp');

        $mail = new PHPMailer();
        $mail->isSMTP();
        $mail->Host = $smtp_host;
        $mail->SMTPAuth = true;
        $mail->Username = $smtp_username;
        $mail->Password = $smtp_password;
        $mail->SMTPSecure = 'tls';
        $mail->Port = $smtp_port;

        return $mail;
    }

    function send_phone($number_phone) {
        $site_settings = pods('email_settings');
        $send_to_email = $site_settings->field('manager_email');

        $mail = set_settings_smtp();

        $mail->setFrom($mail->Username);
        $mail->addAddress($send_to_email);

        $mail->isHTML(true);
        $mail->Subject = 'Заявка на подключение с сайта ' . get_bloginfo('name');

        $content = '<b>Номер телефона: </b>' . $number_phone . "<br>";
        $mail->Body = $content;

        if(!$mail->send()) {
            return false;
        } else {
            return true;
        }
    }