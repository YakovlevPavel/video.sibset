<?php
/**
 * Основные параметры WordPress.
 *
 * Скрипт для создания wp-config.php использует этот файл в процессе
 * установки. Необязательно использовать веб-интерфейс, можно
 * скопировать файл в "wp-config.php" и заполнить значения вручную.
 *
 * Этот файл содержит следующие параметры:
 *
 * * Настройки MySQL
 * * Секретные ключи
 * * Префикс таблиц базы данных
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** Параметры MySQL: Эту информацию можно получить у вашего хостинг-провайдера ** //
/** Имя базы данных для WordPress */
define('DB_NAME', 'vid_sibset');

/** Имя пользователя MySQL */
define('DB_USER', 'root');

/** Пароль к базе данных MySQL */
define('DB_PASSWORD', '123321');

/** Имя сервера MySQL */
define('DB_HOST', 'localhost');

/** Кодировка базы данных для создания таблиц. */
define('DB_CHARSET', 'utf8mb4');

/** Схема сопоставления. Не меняйте, если не уверены. */
define('DB_COLLATE', '');

/**#@+
 * Уникальные ключи и соли для аутентификации.
 *
 * Смените значение каждой константы на уникальную фразу.
 * Можно сгенерировать их с помощью {@link https://api.wordpress.org/secret-key/1.1/salt/ сервиса ключей на WordPress.org}
 * Можно изменить их, чтобы сделать существующие файлы cookies недействительными. Пользователям потребуется авторизоваться снова.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '=Q!_<!`URHqWuC[i{^dRKhg<lUD_CK ;0Yz;WP$,Q-(mo<v2vzr8=mke`0ZAF,GC');
define('SECURE_AUTH_KEY',  'iRx_ft>F+pFNJL,sZTO)b~7MM{OY}k9p{/8nUp%2/YlV~<C6a_V4dR^X$AB3H0gQ');
define('LOGGED_IN_KEY',    'R`99=w+|JX]eh;np7Dv*ZNR@|=n_Ajj-^H<nqV)9*Qhd7k|sfRPKI:-|Hpnx]tkm');
define('NONCE_KEY',        '7LG@/,n#&?QGegrG%SG!chbNy8Km}|A*,:+%-I^Gx@`^nq:ME5EYeAQ}m 1uIaUS');
define('AUTH_SALT',        '-BT/r{:A})wzu}]c%3,ZV&+p=|azE*i^_^,w=Kqx-|({~,9(hk4>G~i)`C>Zfk $');
define('SECURE_AUTH_SALT', '$j+9(o6H7/IF<r/nE{Ce%D#~Hmr&q[ULnrXYBS$nR]|):5Y*tZ1y?O1.TvGdEG{l');
define('LOGGED_IN_SALT',   'jwX<VJC0a[(hLwbU!XERqSY$b*y.}J5;YW0!i@)6pWK<t{(eUbnz7ZT)I2=65xO5');
define('NONCE_SALT',       'R(~-c?59)T:Yl?Ee73[Lz_4h!s|$|.+Hu:<?i6E &61?#&b{VK4`fYMP]B*Dx$27');

/**#@-*/

/**
 * Префикс таблиц в базе данных WordPress.
 *
 * Можно установить несколько сайтов в одну базу данных, если использовать
 * разные префиксы. Пожалуйста, указывайте только цифры, буквы и знак подчеркивания.
 */
$table_prefix  = 'wp_';

/**
 * Для разработчиков: Режим отладки WordPress.
 *
 * Измените это значение на true, чтобы включить отображение уведомлений при разработке.
 * Разработчикам плагинов и тем настоятельно рекомендуется использовать WP_DEBUG
 * в своём рабочем окружении.
 * 
 * Информацию о других отладочных константах можно найти в Кодексе.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', true);

/* Это всё, дальше не редактируем. Успехов! */

/** Абсолютный путь к директории WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Инициализирует переменные WordPress и подключает файлы. */
require_once(ABSPATH . 'wp-settings.php');
